#subfunctions to request different types of business from the Yelp API (doctors, dentists, etc)
#each function will request a specific category of business and parse it to a flat dictionary.

import yelp

CHIROPRACTOR_ENDPOINT = "chiropractors"
COUNSELING_ENDPOINT = "c_and_mh"
DENTIST_ENDPOINT = "dentists"
DOCTOR_ENDPOINT = "physicians"
ER_ENDPOINT = "emergencyrooms"
HOSPITAL_ENDPOINT = "hospitals"
MIDWIFE_ENDPOINT = "midwives"
OPTOMETRIST_ENDPOINT = "optometrists"
PHARMACY_ENDPOINT = "pharmacy"
REHABILITATION_CENTER_ENDPOINT = "rehabilitation_center"
URGENT_CARE_ENDPOINT = "urgent_care"

def val_for_keys( mydict, keylist ):
    try:
        subdict = mydict
        for current_key in keylist:
            subdict = subdict[current_key]
        return subdict
    except:
        return None

def businesses( category, latitude, longitude ):
    try:
        yelpdata = yelp.yelp_request( category , latitude, longitude )
        if (yelpdata['error'] != None) or (yelpdata['businesses'] == []):
            return []

        yelpdata = yelpdata['businesses']
        output_list = []
        for this_business in yelpdata:
            if (this_business['is_closed']):
                continue
            this_dict = {}
            this_dict['category'] =         category
            this_dict['name'] =             this_business['name']
            this_dict['display_phone'] =    val_for_keys(this_business, ['display_phone'])
            this_dict['phone'] =            val_for_keys(this_business, ['phone'])
            this_dict['distance_m'] =       this_business['distance']
            this_dict['image'] =            val_for_keys( this_business, ['image_url'])
            this_dict['address'] =          this_business['location']['display_address']
            this_dict['yelp_link'] =        this_business['mobile_url']
            this_dict['rating'] =           this_business['rating_img_url']
            this_dict['review_count'] =     this_business['review_count']

            output_list.append(this_dict)

        return output_list

    except:
        #raise #debug
        return []


def counseling(latitude, longitude):
    return businesses(COUNSELING_ENDPOINT, latitude, longitude)

def chiropractor(latitude, longitude):
    return businesses(CHIROPRACTOR_ENDPOINT, latitude, longitude)

def dentist(latitude, longitude):
    return businesses(DENTIST_ENDPOINT, latitude, longitude)

def doctor(latitude, longitude):
    return businesses(DOCTOR_ENDPOINT, latitude, longitude)

def ER(latitude, longitude):
    return businesses(ER_ENDPOINT, latitude, longitude)

def hospital(latitude, longitude):
    return businesses(HOSPITAL_ENDPOINT, latitude, longitude)

def midwife(latitude, longitude):
    return businesses(MIDWIFE_ENDPOINT, latitude, longitude)

def optometrist(latitude, longitude):
    return businesses(OPTOMETRIST_ENDPOINT, latitude, longitude)

def pharmacy(latitude, longitude):
    return businesses(PHARMACY_ENDPOINT, latitude, longitude)

def rehab(latitude, longitude):
    return businesses(REHABILITATION_CENTER_ENDPOINT, latitude, longitude)

def urgent_care(latitude, longitude):
    return businesses(URGENT_CARE_ENDPOINT, latitude, longitude)

#def restaurants(latitude, longitude): #just for testing
#    return businesses( 'restaurants', latitude, longitude )
