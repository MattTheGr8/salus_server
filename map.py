#!/usr/bin/python

# for now, we won't worry about caching data / maintaining a database... will just make new requests each time

# imports here
import sys, cgi, cgitb, time, json
import multiprocessing
import map_aeris
import map_reports

# global variables / constants here
data_funcs = [map_aeris.earthquakes, map_aeris.fires, map_aeris.stormcells, map_aeris.stormreports, map_reports.zombies]
data_outputs = []
MAP_TIMEOUT = 15 #in seconds

# function definitions here
def data_output_callback( my_data ):
    data_outputs.append( my_data )


# script proper begins here

# enable logging for debugging purposes (could take this out later)
cgitb.enable(display=0, logdir="pylogs")

# get our parameters from the CGI call
formdata = cgi.FieldStorage()

latitude =          formdata.getvalue("latitude",       "LATITUDE_NULL")
longitude =         formdata.getvalue("longitude",      "LONGITUDE_NULL")
# radius =            formdata.getvalue("radius",         "RADIUS_NULL")
# for the moment, we probably won't do a radius -- will just request max radius from Aeris and send back whatever we get
# - later on, especially when we incorporate more data sources, we may want to add it back (as well as an adjustable
#   date range and potentially other parameters)
# info_types =        formdata.getvalue("info_types",     "INFOTYPES_NULL")
# info_types should be a comma-separated list, e.g. .../map.py?info_types=fire,earthquake
# - but won't worry about info_types for now -- for the moment, we'll just send everything we can get
# - will definitely want to add this in later on so we aren't returning ridiculous numbers of annotations

# basic plan:
# - parse the list of info types
# - for each info type, call a separate Python script (or a subroutine?) that queries appropriate data sources and returns to here
# - once all requests are either collected or timed out, return to iOS app


my_args = (latitude, longitude)
my_pool = multiprocessing.Pool( len(data_funcs) )
async_results = []

for my_func in data_funcs:
    asr_tmp = my_pool.apply_async( my_func, my_args, {}, data_output_callback )
    async_results.append( asr_tmp )

my_pool.close()
#my_pool.join()

pool_timeout_time = time.time() + MAP_TIMEOUT
for my_result in async_results:
    my_result.wait( pool_timeout_time - time.time() )
    if time.time() >= pool_timeout_time:
        my_pool.terminate()
        break

sys.stdout.write('Content-type: text/plain; charset=UTF-8\n\n')

if len(data_outputs) == 0:
    json.dump( [], sys.stdout )
    sys.stdout.write('\n')
    sys.exit()

if len(data_outputs) == 1:
    output_to_write = data_outputs[0]
else:
    output_to_write = sum( data_outputs, [] ) #joins list of lists into a single list [of dictionaries, in this case]
    # sum solution from http://stackoverflow.com/questions/952914/making-a-flat-list-out-of-list-of-lists-in-python

json.dump( output_to_write, sys.stdout )
sys.stdout.write('\n')
sys.exit()
