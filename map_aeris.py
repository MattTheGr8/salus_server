# various subfunctions for requesting different types of source data from the Aeris service (earthquakes, fires, etc.)
# each one requests a single specific data type (using the general-purpose request function in aeris.py) and trims it
#  appropriately to return what map.py is expecting
# each will take two arguments (latitude and longitude, in that order) and return a list of dictionaries
# fields in all dictionaries:
# - event_type: "fire", "earthquake", etc.
# - latitude: float-formatted latitude (of event location, or a central-ish coordinate for distributed area-type events)
# - longitude: same as latitude, but for longitude
# - timestamp: in UNIX epoch time, long integer
# - other: specific data for each event type -- a dictionary with sub-fields (e.g., earthquakes will have "magnitude")

import aeris

EARTHQUAKE_ENDPOINT = "earthquakes"
FIRE_ENDPOINT = "fires"
STORMCELL_ENDPOINT = "stormcells"
STORMREPORT_ENDPOINT = "stormreports"


def val_for_keys( mydict, keylist ):
    try:
        subdict = mydict
        for current_key in keylist:
            subdict = subdict[current_key]
        return subdict
    except:
        return None


def earthquakes( latitude, longitude ):
    try:
        pydata = aeris.aeris_request( EARTHQUAKE_ENDPOINT, latitude, longitude )
        if (pydata['error'] != None) or (not pydata['success']):
            return []

        pydata = pydata['response']
        output_list = []
        for this_event in pydata:
            this_dict = {}
            this_dict['event_type'] = 'earthquake'
            this_dict['latitude'] = this_event['loc']['lat']
            this_dict['longitude'] = this_event['loc']['long']
            this_dict['timestamp'] = this_event['report']['timestamp']

            this_other = {}
            this_other['magnitude'] = val_for_keys(this_event,['report','mag'])

            this_dict['other'] = this_other
            output_list.append(this_dict)

        return output_list

    except:
        return []


def fires( latitude, longitude ):
    try:
        pydata = aeris.aeris_request( FIRE_ENDPOINT, latitude, longitude )
        if (pydata['error'] != None) or (not pydata['success']):
            return []

        pydata = pydata['response']
        output_list = []
        for this_event in pydata:
            this_dict = {}
            this_dict['event_type'] = 'fire'
            this_dict['latitude'] = this_event['loc']['lat']
            this_dict['longitude'] = this_event['loc']['long']
            this_dict['timestamp'] = this_event['report']['timestamp']

            this_other = {}
            this_other['name'] = val_for_keys(this_event,['report','name'])

            this_dict['other'] = this_other
            output_list.append(this_dict)

        return output_list

    except:
        return []


def stormcells(latitude, longitude):
    try:
        pydata = aeris.aeris_request(STORMCELL_ENDPOINT, latitude, longitude)
        if (pydata['error'] != None) or (not pydata['success']):
            return []

        pydata = pydata['response']
        output_list = []
        for this_event in pydata:
            this_dict = {}
            this_dict['event_type'] = 'stormcell'
            this_dict['latitude'] = this_event['loc']['lat']
            this_dict['longitude'] = this_event['loc']['long']
            this_dict['timestamp'] = this_event ['ob']['timestamp']

            this_other = {}
            this_other['tornado_vortex_signature'] = val_for_keys(this_event,['ob','tvs'])
            this_other['severe_hail_probability'] = val_for_keys(this_event,['ob','hail','probSevere'])
            this_other['hail_probability'] = val_for_keys(this_event, ['ob','hail','prob'])
            this_other['hail_size'] = val_for_keys(this_event,['ob','hail','maxSizeIN'])
            this_other['stormcell_speed_KPH'] = val_for_keys(this_event,['ob','movement','speedKMH'])
            this_other['movement_direction_degree'] = val_for_keys(this_event,['ob','movement','dirDEG'])

            this_dict['other'] = this_other
            output_list.append(this_dict)

        return output_list

    except:
        return []

def stormreports(latitude, longitude):
    try:
        pydata = aeris.aeris_request(STORMREPORT_ENDPOINT, latitude, longitude)
        if (pydata['error'] != None) or (not pydata['success']):
            return []
            
        pydata = pydata['response']
        output_list = []
        for this_event in pydata:
            this_dict = {}
            this_dict['event_type'] = 'stormreport'
            this_dict['latitude'] = this_event['loc']['lat']
            this_dict['longitude'] = this_event['loc']['long']
            this_dict['timestamp'] = this_event['report']['timestamp']
            
            this_other = {}
            this_other['type'] = val_for_keys(this_event,['report','type'])
            this_other['reporter'] = val_for_keys(this_event,['report','reporter'])
            this_other['comments'] = val_for_keys(this_event,['report','comments'])
            
            if this_other['type'] == 'heavy rain' or this_other['type'] == 'rain':
                this_other['rainfall_MM'] = val_for_keys(this_event,['report','detail','rainMM'])
            elif this_other['type'] == 'heavy snow' or this_other['type'] == 'snow':
                this_other['snowfall_CM'] = val_for_keys(this_event,['report','detail','snowCM'])
            elif this_other['type'] == 'non-thunderstorm wind gust' or this_other['type'] == 'thunderstorm wind gust' or this_other['type'] == 'non-thunderstorm wind gust' or this_other['type'] == 'extr wind chill' or this_other['type'] == 'marine thunderstorm wind':
                this_other['windspeed_KPH'] = val_for_keys(this_event,['report','detail','windSpeedKPH'])
            elif this_other['type'] == 'hail':
                this_other['hail_size'] = val_for_keys(this_event,['report','detail','hailMM'])
            elif this_other['type'] == 'sleet':
                this_other['sleet_CM'] = val_for_keys(this_event,['report','detail','sleetCM'])
            elif this_other['type'] == 'excessive heat':
                this_other['temp_F'] = val_for_keys(this_event,['report','detail','text'])
            elif this_other['type'] == 'flood' or this_other['type'] == 'dust storm' or this_other['type'] == 'debris flow' or this_other['type'] == 'wildfire' or this_other['type'] == 'rip currents' or this_other['type'] == 'ice storm' or this_other['type'] == 'downburst' or this_other['type'] == 'lightning' or this_other['type'] == 'flash flood' or this_other['type'] == 'tornado' or this_other['type'] == 'water spout' or this_other['type'] == 'funnel cloud' or this_other['type'] == 'freezing rain' or this_other['type'] == 'non-thunderstorm wind damage' or this_other['type'] == 'thunderstorm wind damage':
                pass
            else:
                this_other['details'] = val_for_keys(this_event,['report','detail'])
            
            this_dict['other'] = this_other
            output_list.append(this_dict)
            
        return output_list
    
    except:
        return []