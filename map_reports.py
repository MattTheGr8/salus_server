# for now, just a quickie implementation for demo purposes
# will eventually hold various subfunctions for requesting different types of source data from our own report database
#  (various diseases, types of civil unrest, zombie attacks, etc.)
# each one requests a single specific report type (using the general-purpose request function get_reports(), currently
#  defined in this file but which might get spun out at some later date), and returns its info in the format map.py
#  is expecting
#  - note that this is pretty inefficient right now, as it could involve multiple file-reads as long as our "database"
#    is just a dumb text file, but it will be reasonable enough if we switch to MySQL or whatever... maybe a single
#    query is still better than many queries in theory, but it's probably less of a big deal
# each will take two arguments (latitude and longitude, in that order) and return a list of dictionaries
# fields in all dictionaries:
# - event_type: "riots", "zombies", etc.
# - latitude: float-formatted latitude (of event location, or a central-ish coordinate for distributed area-type events)
# - longitude: same as latitude, but for longitude
# - timestamp: in UNIX epoch time, long integer
# - other: specific data for each event type -- a dictionary with sub-fields

ZOMBIES_REPORT_CODE = "zombieism"
DEFAULT_RADIUS_M = 804672 # 500 miles in meters
DB_FNAME = 'report_db_simple.txt'


def get_reports( report_code, latitude, longitude, radius ):

    # again, being super-lazy for now -- supposed to return only codes within a certain radius of
    #  a given latitude/longitude as specified in the argument list. But for now, we'll just return
    #  everything....

    # assuming radius will be in meters? But since we're not actually using it at the moment, I guess
    #  we could change later -- just have to remember to update DEFAULT_RADIUS_M accordingly

    try:
        filehandle = open(DB_FNAME, 'r')
        all_lines = filehandle.readlines()
        filehandle.close()
    except:
        return []

    output_list = []

    for this_line in all_lines:
        this_line = this_line.strip( '\n' )
        this_line_cells = this_line.split( '\t' )
        this_report_code = this_line_cells[0].strip()

        if (this_report_code == report_code):
            this_latitude = this_line_cells[1].strip()
            this_longitude = this_line_cells[2].strip()
            this_timestamp = this_line_cells[3].strip()

            this_latitude = float(this_latitude)
            this_longitude = float(this_longitude)
            this_timestamp = int(this_timestamp)

            this_dict = {}
            this_dict['event_type'] = report_code
            this_dict['latitude'] = this_latitude
            this_dict['longitude'] = this_longitude
            this_dict['timestamp'] = this_timestamp
            this_dict['other'] = {}

            output_list.append(this_dict)

    return output_list


def zombies( latitude, longitude ):

    return get_reports( ZOMBIES_REPORT_CODE, latitude, longitude, DEFAULT_RADIUS_M )
