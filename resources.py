#!/usr/bin/python

# very similar to map.py to start, at least... will see how they diverge as they continue to develop
# for now, we won't worry about caching data / maintaining a database... will just make new requests each time
# (and at least for now, Yelp for one isn't allowing us to cache, per their TOS)

# imports here
import sys, cgi, cgitb, time, json
import multiprocessing
import resources_yelp

# global variables / constants here
data_funcs = [resources_yelp.counseling, resources_yelp.chiropractor, resources_yelp.dentist, resources_yelp.doctor,
              resources_yelp.ER, resources_yelp.hospital, resources_yelp.midwife, resources_yelp.optometrist,
              resources_yelp.pharmacy, resources_yelp.rehab, resources_yelp.urgent_care]
#data_funcs = [resources_yelp.restaurants] #just for testing
data_outputs = []
RESOURCES_TIMEOUT = 15 #in seconds

# function definitions here
def data_output_callback( my_data ):
    data_outputs.append( my_data )


# script proper begins here

# enable logging for debugging purposes (could take this out later)
cgitb.enable(display=0, logdir="pylogs")

# get our parameters from the CGI call
formdata = cgi.FieldStorage()

latitude =          formdata.getvalue("latitude",       "LATITUDE_NULL")
longitude =         formdata.getvalue("longitude",      "LONGITUDE_NULL")

# basic plan:
# - parse the list of categories
# - for each category, call a separate Python script (or a subroutine?) that queries appropriate data sources and returns to here
# - once all requests are either collected or timed out, return to iOS app


my_args = (latitude, longitude)
my_pool = multiprocessing.Pool( len(data_funcs) )
async_results = []

for my_func in data_funcs:
    asr_tmp = my_pool.apply_async( my_func, my_args, {}, data_output_callback )
    async_results.append( asr_tmp )

my_pool.close()
#my_pool.join()

pool_timeout_time = time.time() + RESOURCES_TIMEOUT
for my_result in async_results:
    my_result.wait( pool_timeout_time - time.time() )
    if time.time() >= pool_timeout_time:
        my_pool.terminate()
        break

sys.stdout.write('Content-type: text/plain; charset=UTF-8\n\n')

if len(data_outputs) == 0:
    json.dump( [], sys.stdout )
    sys.stdout.write('\n')
    sys.exit()

if len(data_outputs) == 1:
    output_to_write = data_outputs[0]
else:
    output_to_write = sum( data_outputs, [] ) #joins list of lists into a single list [of dictionaries, in this case]
    # sum solution from http://stackoverflow.com/questions/952914/making-a-flat-list-out-of-list-of-lists-in-python

json.dump( output_to_write, sys.stdout )
sys.stdout.write('\n')
sys.exit()
