# make a request from the Aeris server and return a Python dictionary object
# - everything from Aeris is returned; it is the responsibility of the calling function to trim down
#   the received data if so desired
# - lots of the parameters and such here are Aeris-specific stuff; they are documented a bit here,
#   but for more complete information it is probably worth checking out the full API docs at
#   http://www.hamweather.com/support/documentation/aeris/endpoints/

import datetime, httplib, urllib, json

PLACE_TOKEN = "p"
LIMIT_TOKEN = "limit"
RADIUS_TOKEN = "radius"
FROMDATE_TOKEN = "from"
CLIENT_ID_TOKEN = "client_id"
CLIENT_SECRET_TOKEN = "client_secret"
RESULTS_LIMIT = 250         # set by Aeris API
CLIENT_ID = "G2NReWFl0NBQ9iz38dMZZ"
CLIENT_SECRET = "Rw3r8Sz8kaSrOp0cSrmIwwUXUfdVwCoDRGrmep8F"
AERIS_SERVER = "api.aerisapi.com"

def aeris_request( endpoint, latitude, longitude, action="closest", radius="500mi", daysback=7 ):
    # endpoint: must be a string containing the name of a valid Aeris endpoint
    # - "advisories", "earthquakes", "fires", "stormcells", "stormreports" should all work similarly
    # - other options might work too, but these are most likely to be relevant to our interests
    # action: must be a string representing an Aeris action parameter
    # - currently "closest" (the default) is the only real option
    # - ("within" would probably work too, but "closest" is our best bet for now)
    # radius: must be a string representing a distance in Aeris-acceptable format (see documentation
    #   linked at top of file)
    # lastdays: must be an integer representing how many days back (relative to today) we want to
    #   search for our request
    # - 1 and up should definitely work; need to check to see if 0 (meaning only today?) also works
    # general note: not a TON of argument checking at this point... so if the passed-in arguments
    #   don't fit the specified format, you're gonna have a bad time
    #
    # returns: a Python dictionary object representing whatever JSON the Aeris API spit back; such
    #   a dictionary could be converted back to text format (for passing back to an app, say) with
    #   json.dumps(), if one were so inclined

    latlong_str = "{0},{1}".format( latitude, longitude )
    today_date = datetime.date.today()
    time_delta = datetime.timedelta( days=daysback )
    from_date = today_date - time_delta
    fromdate_str = from_date.isoformat()

    params_dict = { PLACE_TOKEN: latlong_str,
                    LIMIT_TOKEN: RESULTS_LIMIT,
                    RADIUS_TOKEN: radius,
                    FROMDATE_TOKEN: fromdate_str,
                    CLIENT_ID_TOKEN: CLIENT_ID,
                    CLIENT_SECRET_TOKEN: CLIENT_SECRET
                  }

    url_params = urllib.urlencode( params_dict )
    my_url = '/{0}/{1}?{2}'.format( endpoint, action, url_params )
    try:
        http_connection = httplib.HTTPConnection(AERIS_SERVER)
        http_connection.request("POST",my_url)
        http_response = http_connection.getresponse()
        my_data = http_response.read()
        http_connection.close()
        my_obj = json.loads( my_data )
    except:
        my_obj = {'error': "Could not access server"}
        #http_connection.close()

    return my_obj
