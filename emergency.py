#!/usr/bin/python

#import sys, json;

db_fname = 'emergency_db_simple.txt'
# file format: tab-delimited text
# fields: Country, Ambulance, Fire, Police, ISO country code
# first line has human-readable column headers; start reading from second

import sys, cgi, cgitb, json

cgitb.enable(display=0, logdir="pylogs")
formdata = cgi.FieldStorage()
country_code = formdata.getvalue("country_code",      "COUNTRYCODE_NULL")

sys.stdout.write('Content-type: text/plain; charset=UTF-8\n\n')
#with open("json_dump_temp.txt","a") as dumpfile:
#    dumpfile.write('Content-type: text/plain; charset=UTF-8\n\n')

try:
    filehandle = open(db_fname, 'r')
    all_lines = filehandle.readlines()
    filehandle.close()
except:
    all_info_dict = {}
    json.dump( all_info_dict, sys.stdout )
    sys.stdout.write('\n')
#    with open("json_dump_temp.txt","a") as dumpfile:
#        json.dump( all_info_dict, dumpfile )
#        dumpfile.write('\n')
    sys.exit()

for this_line in all_lines:
    this_line = this_line.strip( '\n' )
    this_line_cells = this_line.split( '\t' )
    this_countrycode = this_line_cells[4].strip()
    if (this_countrycode == country_code):
        this_location = this_line_cells[0].strip()
        this_ambulance = this_line_cells[1].strip()
        this_fire = this_line_cells[2].strip()
        this_police = this_line_cells[3].strip()
        if (this_ambulance == this_fire and this_fire == this_police): #all numbers the same
            numbers_dict = {'all': this_ambulance}
        else:
            numbers_dict = {}
            if (not this_ambulance == ""):
                numbers_dict['ambulance'] = this_ambulance
            if (not this_fire == ""):
                numbers_dict['fire'] = this_fire
            if (not this_police == ""):
                numbers_dict['police'] = this_police
            if (not numbers_dict): #somehow no numbers? if so, return empty dictionary
                all_info_dict = {}
                json.dump( all_info_dict, sys.stdout )
                sys.stdout.write('\n')
                sys.exit()
        all_info_dict = {'location': this_location, 'numbers': numbers_dict}
        json.dump( all_info_dict, sys.stdout )
        sys.stdout.write('\n')
        #with open("json_dump_temp.txt","a") as dumpfile:
        #    json.dump( all_info_dict, dumpfile )
        #    dumpfile.write('\n')
        sys.exit()

all_info_dict = {}
json.dump( all_info_dict, sys.stdout )
sys.stdout.write('\n')
#with open("json_dump_temp.txt","a") as dumpfile:
#    json.dump( all_info_dict, dumpfile )
#    dumpfile.write('\n')

sys.exit()
