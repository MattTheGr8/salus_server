# make a request from the Yelp server and return a Python dictionary object
# - everything from Yelp is returned; it is the responsibility of the calling function to trim down
#   the received data if so desired
# - lots of the parameters and such here are Yelp-specific stuff; they are documented a bit here,
#   but for more complete information it is probably worth checking out the full API docs at
#   http://www.yelp.com/developers/documentation


import oauth2, urllib, json, urllib2


CATEGORY_TOKEN = 'category_filter'
LAT_LONG_TOKEN = 'll'

#Security stuff to connect to the Yelp API
CONSUMER_KEY = "yJASjKbk7cBmEzODIZG74A"
CONSUMER_SECRET = "LfK5YHh_78rUAVd2w78rgeP44aA"
TOKEN = "eknx-ByZsSCyvUIwKD6gBo3SD_uBV82E"
TOKEN_SECRET = "TcHZr-8IP6uI7i1VQWSAr1EQlMU"
API_HOST = "api.yelp.com"

SEARCH_PATH = '/v2/search/'



def yelp_request( category_term, latitude, longitude ):
    # document parameters soon, after we get the function working
    #
    #
    # returns: a Python dictionary object representing whatever JSON the Yelp API spit back; such
    #   a dictionary could be converted back to text format (for passing back to an app, say) with
    #   json.dumps(), if one were so inclined

    latlong_str = "{0},{1}".format( latitude, longitude ) #might need a space in between? but let's try it like this first

    params_dict = { CATEGORY_TOKEN: category_term,
                    LAT_LONG_TOKEN: latlong_str
                  }

    #url_params = urllib.urlencode( params_dict )
    # seem to be various issues with OAuth and URL encoding... might have to do with different versions of things?
    #  - regardless, this version appears to work for now on both server & Mac
    #  - (with the line above removed and the oauth2.Request line changed below to add params back in)
    #  - details on the fix made in the previous commit, for reference:
    #  - https://github.com/dennyglee/yelp-api/commit/25c5b1838b943e59ddf8a7aca1fb224f45ef546a
    my_url = 'http://{0}{1}?'.format(API_HOST, SEARCH_PATH)

    consumer = oauth2.Consumer(CONSUMER_KEY, CONSUMER_SECRET)
    oauth_request = oauth2.Request('GET', url=my_url, parameters=params_dict )
    oauth_request.update(
        {'oauth_nonce': oauth2.generate_nonce(),
         'oauth_timestamp': oauth2.generate_timestamp(),
         'oauth_token': TOKEN,
         'oauth_consumer_key': CONSUMER_KEY
        }
    )
    token = oauth2.Token(TOKEN, TOKEN_SECRET)
    oauth_request.sign_request(oauth2.SignatureMethod_HMAC_SHA1(), consumer, token)
    signed_url = oauth_request.to_url()

    try:
        http_connection = urllib2.urlopen(signed_url, None)
        my_data = http_connection.read()
        http_connection.close()
        my_obj = json.loads( my_data )
        my_obj['error'] = None
    except:
        my_obj = {'error': "Could not access server"}
        #http_connection.close()

    return my_obj
