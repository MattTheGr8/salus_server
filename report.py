#!/usr/bin/python

# for now, just a placeholder to test iOS app...

# imports here
import sys, cgi, cgitb, os.path, time
# etc.


# global variables / constants here

db_fname = 'report_db_simple.txt'
# file format: tab-delimited text
# (obviously super-basic for now -- just for testing/demo purposes)
# fields: report code (string), latitude (floating-point), longitude (floating-point), timestamp (UNIX epoch time, long integer)
# no human-readable column headers for now; can start reading from first line

lockfile_fname  = 'report_db_lockfile'
# also doing this the super-ghetto way for now


# function definitions here
# (fill in later)



# script proper begins here

# enable logging for debugging purposes (could take this out later)
cgitb.enable(display=0, logdir="pylogs")

# get our parameters from the CGI call
formdata = cgi.FieldStorage()

report_type =       formdata.getvalue("type",           "TYPE_NULL")
latitude =          formdata.getvalue("latitude",       "LATITUDE_NULL")
longitude =         formdata.getvalue("longitude",      "LONGITUDE_NULL")


sys.stdout.write('Content-type: text/plain; charset=UTF-8\n\n')

timestamp_str = '{0}'.format( int(round( time.time() )) )
line_to_print = '{0}\t{1}\t{2}\t{3}\n'.format( report_type, latitude, longitude, timestamp_str )

#check lock status
try_count = 0
lockfile_exists = os.path.isfile( lockfile_fname )
while lockfile_exists:
    time.sleep(0.2)
    try_count += 1
    if try_count > 10:
        sys.stdout.write('error')
        sys.exit()
    lockfile_exists = os.path.isfile( lockfile_fname )

open(lockfile_fname, 'a').close() #creates lockfile and immediately closes it

try:
    filehandle = open(db_fname, 'a')
    filehandle.write(line_to_print)
    filehandle.close()
    os.remove(lockfile_fname)
except:
    sys.stdout.write('error')
    os.remove(lockfile_fname)
    filehandle.close()
    sys.exit()

sys.stdout.write('OK')
sys.exit()
