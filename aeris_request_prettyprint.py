import httplib, urllib, json

# see http://www.hamweather.com/support/documentation/aeris/endpoints/ for detailed API documentation
PLACE_TOKEN = "p"
LIMIT_TOKEN = "limit"
RADIUS_TOKEN = "radius"
FROMDATE_TOKEN = "from"
CLIENT_ID_TOKEN = "client_id"
CLIENT_SECRET_TOKEN = "client_secret"

RESULTS_LIMIT = 250         # set by Aeris API
CLIENT_ID = "G2NReWFl0NBQ9iz38dMZZ"
CLIENT_SECRET = "Rw3r8Sz8kaSrOp0cSrmIwwUXUfdVwCoDRGrmep8F"
AERIS_SERVER = "api.aerisapi.com"

#my_url    = "/fires/closest?p=99502&limit=100&from=2014/05/01&radius=150mi&client_id=G2NReWFl0NBQ9iz38dMZZ&client_secret=Rw3r8Sz8kaSrOp0cSrmIwwUXUfdVwCoDRGrmep8F"

my_endpoint = "earthquakes"     # "advisories", "earthquakes", "fires", "stormcells", "stormreports" should all work similarly
                                #  other options might work too, but these are most likely to be relevant to our interests
my_action = "closest"           # currently the only real option; "within" would probably work too, but "closest" is likely our best option for now
my_latlong = "61.16,-149.97"    # somewhere in Anchorage, for now... zipcode 99502
my_radius = "500mi"             # number+units; default radius is 25 miles; default unit is meters
my_fromdate = "2014-06-22"      # can be a date string, UNIX timestamp (straight numbers), or various other date strings...
                                #  n.b.: "to" dates area also supported, but not using for now... assuming always "to" present moment


###################################

params_dict = { PLACE_TOKEN: my_latlong,
                LIMIT_TOKEN: RESULTS_LIMIT,
                RADIUS_TOKEN: my_radius,
                FROMDATE_TOKEN: my_fromdate,
                CLIENT_ID_TOKEN: CLIENT_ID,
                CLIENT_SECRET_TOKEN: CLIENT_SECRET
              }
url_params = urllib.urlencode( params_dict )
my_url = '/{0}/{1}?{2}'.format(my_endpoint, my_action, url_params)

http_connection = httplib.HTTPConnection(AERIS_SERVER)
http_connection.request("POST",my_url)

http_response = http_connection.getresponse()

my_data = http_response.read()

http_connection.close()

my_obj = json.loads( my_data )

print json.dumps(my_obj,sort_keys=True,indent=4,separators=(',', ': '))
#print json.dumps(my_obj, sort_keys=True)
